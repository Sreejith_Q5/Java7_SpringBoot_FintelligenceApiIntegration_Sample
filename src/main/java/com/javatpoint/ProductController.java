package com.javatpoint;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;

//For Appscook method
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;


@RestController
public class ProductController 
{
	@Autowired
	//mapping the getProduct() method to /product
	@GetMapping(value = "/product")
	public String getProduct() 
	{
		String result = "";

		// Rest template method
		//String uri = "https://fintelligence.live/webapis/UserLogin/GetReleaseVersion";
		//RestTemplate restTemplate = new RestTemplate();
		//result = restTemplate.getForObject(uri, String.class);


		//httpsclient method
		//		try {
		//
		//	        DefaultHttpClient httpClient = new DefaultHttpClient();
		//	        HttpGet getRequest = new HttpGet(
		//	            "https://fintelligence.live/webapis/UserLogin/GetReleaseVersion");
		//	        getRequest.addHeader("accept", "application/json");
		//
		//	        HttpResponse response = httpClient.execute(getRequest);
		//
		//	        if (response.getStatusLine().getStatusCode() != 200) {
		//	            throw new RuntimeException("Failed : HTTP error code : "
		//	               + response.getStatusLine().getStatusCode());
		//	        }
		//
		//	        BufferedReader br = new BufferedReader(
		//	                         new InputStreamReader((response.getEntity().getContent())));
		//
		//	        String output;
		//	        System.out.println("Output from Server .... \n");
		//	        while ((output = br.readLine()) != null) {
		//	            System.out.println(output);
		//	        }
		//
		//	        httpClient.getConnectionManager().shutdown();
		//
		//	      } catch (ClientProtocolException e) {
		//	    
		//	        e.printStackTrace();
		//
		//	      } catch (IOException e) {
		//	    
		//	        e.printStackTrace();
		//	      }


		// Method received from appscook
		//Get

		GetMethod method = new GetMethod();
		HttpClient client = new HttpClient();
		String url="https://fintelligence.live/webapis/UserLogin/GetReleaseVersion";
		method = new GetMethod(url);
		try {
			client.executeMethod(method);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(method.getStatusCode());
		if (method.getStatusCode() == 200) {
			try {
				result = method.getResponseBodyAsString();
				System.out.println(result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Post
		//	HttpClient httpClient = new HttpClient();
		//	PostMethod postMethod = new PostMethod(
		//			"https://fintelligence.live/webapis/Organization/CreateOrganisationForParentConnect");
		//	postMethod.addRequestHeader("accept", "application/json");
		//
		//	postMethod.addParameter("emailID", "sreeji.pa@gmail.com");
		//	postMethod.addParameter("address", "");
		//	postMethod.addParameter("contactNumber", "");
		//	postMethod.addParameter("gstin", "NA");
		//	postMethod.addParameter("organizationLogo", "");
		//	postMethod.addParameter("organizationName", "");
		//	postMethod.addParameter("pan", "NA");
		//	postMethod.addParameter("schoolCode", "0405");
		//	postMethod.addParameter("password", "123456");
		//	postMethod.addParameter("username", "9349667475");
		//	postMethod.addParameter("key", "qwe123");
		//	 try {
		//	        httpClient.executeMethod(postMethod);
		//	    } catch (HttpException e) {
		//	        e.printStackTrace();
		//	    } catch (IOException e) {
		//	        e.printStackTrace();
		//	    }
		//	    String resp = null;
		//	    System.out.println(postMethod.getStatusCode());
		//	    if (postMethod.getStatusCode() == 200) {
		//	        try {
		//	        	resp = postMethod.getResponseBodyAsString();
		//	        	System.out.println(resp);
		//			} catch (IOException e) {
		//				e.printStackTrace();
		//			}
		//	    }

		return result;
	}
}
